#include "pstat.h"
#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"

int main (void) {
	struct pstat stat;
	getpinfo(&stat);
	int i;
	for (i = 0 ; i < NPROC ; i++){
		printf(1,"INUSE: %d ,PID: %d , HTICKS: %d ,LTICKS: %d \n"
	      , stat.inuse[i], stat.pid[i], stat.hticks[i], stat.lticks[i]);
	}
exit();
return 0;
}

